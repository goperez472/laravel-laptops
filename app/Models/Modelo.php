<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Modelo extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $fillable= ['nombre', 'sistema_operativo', 'procesador', 'graficos', 'memoria', 'id_marca'];
}
