<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Marcas;

class MarcasController extends Controller
{
    
    public function index()
    {
        $marcas = Marcas::all();
        return view('marcas',compact('marcas'));
    }

    public function create()
    {
        //
    }

    
    public function store(Request $request)
    {
        $marca = new Marcas($request->input());
        $marca->saveOrFail();
        return redirect('marcas');
        $marca = new Marcas($request->input());
        $marca->save();
        return redirect()->route('marcas.index')
        ->with('success','Marca guardada');
    }

    
    public function show(string $id)
    {
        $marca = Marcas::find($id);
        return view('editarMarca',compact('marca'));
    }

    
    public function edit(string $id)
    {
        //
    }

   
    public function update(Request $request, string $id)
    {
        $marca = Marcas::find($id);
        $marca->fill($request->input())->saveOrFail();
        return redirect('marcas');
    }

   
    public function destroy(string $id)
    {
        $marca = Marcas::find($id);
        $marca->delete();
        return redirect('marcas');
    }
}
