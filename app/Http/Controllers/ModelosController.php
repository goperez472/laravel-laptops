<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Modelo;
use App\Models\Marcas;

class ModelosController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $modelos = Modelo::select('modelos.id','nombre','sistema_operativo','procesador','graficos','memoria','id_marca','marca')
        ->join('marcas','marcas.id','=','modelos.id_marca')->get();
        $marcas = Marcas::all();
        return view('modelos',compact('modelos','marcas'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $modelos = new Modelo($request->input());
        $modelos->saveOrFail();
        return redirect()->route('modelos.index')->with('success', ' Modelo guardado');

    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $modelos = Modelo::find($id);
        $marca = Marcas::all();
        return view('editarModelos',compact('modelos','marca'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $modelos = Modelo::find($id);
        $modelos->fill($request->input())->saveOrFail();
        return redirect()->route('modelos.index')->with('success', ' Modelo Actualizado');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $modelos = Modelo::find($id);
        $modelos->delete();
        return redirect('modelos');
    }
}
