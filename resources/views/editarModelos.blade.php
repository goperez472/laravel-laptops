@extends('plantilla')
@section('contenido')
    <div class="row mt-3">
        <div class="col-md-6 offset-md-3">
            <div class="card">
                <div class="card-header bg-dark text-white">EDITAR MARCA</div>
                <div class="card-body">
                    <form id="frmMarcas" method="POST" action={{url("modelos",[$modelos])}}>
                        @csrf
                        @method('PUT')
                        <div class="input-group mb-3">
                            <span class="input-group-text"><i class="fa-solid fa-desktop"></i></span>
                            <input type="text" name="nombre" value="{{ $modelos->nombre}}" class="form-control" maxlength="50" placeholder="Nombre del modelo" required>
                        </div>
                        <div class="input-group mb-3">
                            <span class="input-group-text"><i class="fa-brands fa-windows"></i></span>
                            <input type="text" name="sistema_operativo" value="{{ $modelos->sistema_operativo}}" class="form-control" maxlength="50" placeholder="Sistema Operativo" required>
                        </div>
                        <div class="input-group mb-3">
                            <span class="input-group-text"><i class="fa-solid fa-microchip"></i></span>
                            <input type="text" name="procesador" value="{{ $modelos->procesador}}" class="form-control" maxlength="50" placeholder="Procesador" required>
                        </div>
                        <div class="input-group mb-3">
                            <span class="input-group-text"><i class="fa-solid fa-panorama"></i></span>
                            <input type="text" name="graficos" value="{{ $modelos->graficos}}" class="form-control" maxlength="50" placeholder="Graficos" required>
                        </div>
                        <div class="input-group mb-3">
                            <span class="input-group-text"><i class="fa-solid fa-memory"></i></span>
                            <input type="text" name="memoria" value="{{ $modelos->memoria}}" class="form-control" maxlength="50" placeholder="Memoria" required>
                        </div>
                        <div class="input-group mb-3">
                            <span class="input-group-text"><i class="fa-solid fa-laptop"></i></span>
                            <select name="id_marca" class="form-select" required>
                                <option value="">Marca</option>
                                @foreach($marca as $row)
                                    @if($row->id == $modelos->id_marca)
                                        <option value="{{ $row->id }}" selected="selected">{{ $row->marca }}</option>
                                    @else
                                        <option value="{{ $row->id }}">{{ $row->marca }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="d-grid col-6 mx-auto">
                            <button class="btn btn-success"><i class="fa-solid fa-floppy-disk"></i> Guardar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection