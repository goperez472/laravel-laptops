@extends('plantilla')
@section('contenido')

@if($mensaje = Session::get('success'))
    <div class="row" id="divok">
        <div class="col-md-6 offset-md-3">
            <div class="alert alert-success">
                <i class="fa-solid fa-check"></i>  {{$mensaje}}
            </div>
        </div>
</div>
@endif

    <div class="row">
        <div class="col-md-4 offset-md-4">
            <div class="d-grid mx-auto">
                <button class="btn btn-success mt-3" data-bs-toggle="modal" data-bs-target="#modalMarcas">
                    <i class="fa-solid fa-circle-plus"></i> Añadir
                </button>
            </div>
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-12 col-lg-8 offset-0 offset-lg-2">
            <div class="table-responsive">
                <table class="table table-dark table-hover ">
                    <thead><tr><th>#</th><th>Modelo</th><th>Sistema Operativo</th><th>Procesador</th><th>Graficos</th><th>Memoria</th><th>Marca</th><th>Editar</th><th>Eliminar</th></tr></thead>
                    <tbody class="table-group-divider">
                        @php $i=1; @endphp
                        @foreach ($modelos as $row)
                            <tr>
                                <td>{{ $i++ }}</td>
                                <td>{{ $row->nombre }}</td>
                                <td>{{ $row->sistema_operativo }}</td>
                                <td>{{ $row->procesador }}</td>
                                <td>{{ $row->graficos }}</td>
                                <td>{{ $row->memoria }}</td>
                                <td>{{ $row->marca }}</td>
                                <td>
                                    <a href="{{ url('modelos',[$row]) }}" class="btn btn-warning"><i class="fa-solid fa-edit"></i></a>
                                </td>
                                <td>
                                    <form method="POST" action="{{ url('modelos',[$row]) }}">
                                        @method("delete")
                                        @csrf
                                        <button class="btn btn-danger"><i class="fa-solid fa-trash"></i></button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalMarcas" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
                <label class="h5" id="titulo_modal">Añadir Modelos</label>
              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form id="frmMarcas" method="POST" action={{url("modelos")}}>
                    @csrf
                    <div class="input-group mb-3">
                        <span class="input-group-text"><i class="fa-solid fa-desktop"></i></span>
                        <input type="text" name="nombre" class="form-control" maxlength="50" placeholder="Nombre del modelo" required>
                    </div>
                    <div class="input-group mb-3">
                        <span class="input-group-text"><i class="fa-brands fa-windows"></i></span>
                        <input type="text" name="sistema_operativo" class="form-control" maxlength="50" placeholder="Sistema Operativo" required>
                    </div>
                    <div class="input-group mb-3">
                        <span class="input-group-text"><i class="fa-solid fa-microchip"></i></span>
                        <input type="text" name="procesador" class="form-control" maxlength="50" placeholder="Procesador" required>
                    </div>
                    <div class="input-group mb-3">
                        <span class="input-group-text"><i class="fa-solid fa-panorama"></i></span>
                        <input type="text" name="graficos" class="form-control" maxlength="50" placeholder="Graficos" required>
                    </div>
                    <div class="input-group mb-3">
                        <span class="input-group-text"><i class="fa-solid fa-memory"></i></span>
                        <input type="text" name="memoria" class="form-control" maxlength="50" placeholder="Memoria" required>
                    </div>
                    <div class="input-group mb-3">
                        <span class="input-group-text"><i class="fa-solid fa-laptop"></i></span>
                       <select name="id_marca" class="form-select" required>
                            <option value="">Marca</option>
                            @foreach($marcas as $row)
                            <option value="{{ $row->id }}">{{ $row->marca }}</option>
                            @endforeach
                       </select>
                    </div>
                    <div class="d-grid col-6 mx-auto">
                        <button class="btn btn-success"><i class="fa-solid fa-floppy-disk"></i> Guardar</button>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
              <button type="button" id="btnCerrar" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
            </div>
          </div>
        </div>
      </div>


      <div class="modal" tabindex="-1" id="modalEliminar">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title"><i class="fa-solid fa-warning"> </i> Eliminar</h5>
              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
              <p>¿Segúro de eliminar la pelicula <label id="lbl_nombre"> </label> ?</p>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-bs-dismiss="modal"><i class="fa-solid fa-ban"></i> Cancelar</button>
              <button id="btnEliminar" type="button" class="btn btn-warning"><i class="fa-solid fa-check"></i> Si, eliminar pelicula</button>
            </div>
          </div>
        </div>
    </div>

@endsection
@section('js')
@vite('resources/js/alertas.js')
@endsection